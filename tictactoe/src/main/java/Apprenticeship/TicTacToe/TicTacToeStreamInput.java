package Apprenticeship.TicTacToe;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Scanner;

public class TicTacToeStreamInput {

	protected final TicTacToe game;
	
	private final InputStream in;
	protected final OutputStream out;
	
	TicTacToeStreamInput(TicTacToe game, InputStream in, OutputStream out){
		
		this.game = game;
		this.in = in;
		this.out = out;
	}
	
	public void takeTurn() {
		if(game.getGameStatus() != Status.normal) {
			return;
		}
		
		List<Integer> openPositions = game.getOpenPositions();
		
		int position = -1;
		
        while(!openPositions.contains(position)) {
       	
        		try {
        			out.write("Your turn. Please enter position 0-8: ".getBytes());
        		}
        		catch(Exception e) {
        		
        		}
        	
        		Scanner scanner = new Scanner(in);
        		String coords = scanner.next();
        		position = Integer.parseInt(coords.substring(0,1));
        	}
        
        
        game.move(position);
        
    }
	
	public boolean replayPrompt() {
		
		try {
			out.write("Do you want to play again? y/n: ".getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Scanner scanner = new Scanner(in);
		
		if(scanner.next().equals("y")) {
    		return true;
    	} else {
    		return false;
    	}
		
	}
	
}
