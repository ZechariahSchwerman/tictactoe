package Apprenticeship.TicTacToe;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Computer extends TicTacToeStreamInput{

	Computer(TicTacToe game, InputStream in, OutputStream out) {
		super(game, in, out);
		// TODO Auto-generated constructor stub
	}

	public void takeTurn() {
		
		if(game.getGameStatus() != Status.normal) {
			return;
		}
		
		List<Integer> openPositions = game.getOpenPositions();
		
		Random rand = new Random();
		
		int position = rand.nextInt(openPositions.size());
		
		game.move(openPositions.get(position));
		
		try {
			out.write(("Computer moved at position " + openPositions.get(position) + "\n").getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
