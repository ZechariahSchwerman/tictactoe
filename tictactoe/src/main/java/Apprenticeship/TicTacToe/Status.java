package Apprenticeship.TicTacToe;

public enum Status {
	normal,
	gridFull,
	occupied,
	x,
	o
}
