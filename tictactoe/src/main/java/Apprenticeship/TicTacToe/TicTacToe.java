package Apprenticeship.TicTacToe;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class TicTacToe
{
	/*
	 * The same status enum is used for representing:
	 * grid location occupation
	 * current player
	 * win/game end state
	 * 
	 * This is likely going to be a problem.
	 */
	private Status[][] grid = {{Status.normal, Status.normal, Status.normal},
							   {Status.normal, Status.normal, Status.normal},
							   {Status.normal, Status.normal, Status.normal}};
	private Status currentPlayer = Status.x;
	
	private File log = new File("game.log");

	private Status gameStatus = Status.normal;
	
	public static void main(String[] args) {
        
		
		TicTacToe game = new TicTacToe();
		
		TicTacToeStreamOutput out = new TicTacToeStreamOutput(game, System.out);
		TicTacToeStreamInput in = new TicTacToeStreamInput(game, System.in, System.out);
		
		Computer computer = new Computer(game, System.in, System.out);
		
		while(game.getGameStatus() == Status.normal) {
		
			
			
			out.render();
			
			while(game.getGameStatus() == Status.normal) {
				in.takeTurn();
				computer.takeTurn();
				out.render();
			}
			
			if(in.replayPrompt()) {
				game.reset();
			}
		
		}
        
    }
	
	TicTacToe(){

		try {
			Files.write(log.toPath(), "game start\n".getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void reset() {
		grid = new Status[][] {{Status.normal, Status.normal, Status.normal},
				   {Status.normal, Status.normal, Status.normal},
				   {Status.normal, Status.normal, Status.normal}};
		
		currentPlayer = Status.x;
		
		gameStatus = Status.normal;
		
	}
	
	
	public int getGridSize() {
		return grid.length;
	}
	
	public List<Integer> getOpenPositions() {
		
		List<Integer> openPositions = new ArrayList<Integer>();
		
		for(int j = 0; j < grid.length; j++) {
			for(int i = 0; i < grid.length; i++) {
				
				if(grid[i][j] == Status.normal) {
					openPositions.add(i + j*grid.length);
				}
				
			}
		}
		
		return openPositions;
	}
	
	public Status getLinearPositionStatus(int position) {
		return grid[position/grid.length][position%grid.length];
	}
	
	public Status getXYPositionStatus(int x, int y) {
		return grid[x][y];
	}
	
	private int computerTurn() {
		Random rand = new Random();
		
		int position = rand.nextInt(9);
		
		while(move(position) == Status.occupied) {
			position++;
		}
		
		
		return position;
		
	}
	
	public Status getGameStatus() {
		return gameStatus;
	}
	
	public Status move(int position) {

		int y = position / grid.length;
		int x = position % grid.length;
		
		if(gameStatus != gameStatus.normal) {
			return gameStatus;
		}
		
		if(grid[x][y] != gameStatus.normal) {
			//probably should not refuse move silently
			//when player tries to use an occupied space
			return gameStatus.occupied;
		}
		
		grid[x][y] = currentPlayer;
		
		if(currentPlayer == Status.x) {
			currentPlayer = Status.o;
		}else {
			currentPlayer = Status.x;
		}
		
		gameStatus = checkForEnd();
		
		return gameStatus;
	}
	
	
	private Status checkForEnd() {
		
		if(checkHV() != Status.normal) {
			return checkHV();
		}
		if(checkDiagonals() != Status.normal) {
			return checkDiagonals();
		}
		if(checkGridFull()) {
			return Status.gridFull;
		}
		
		return Status.normal;
		
	}
	
	
	private Status checkHV() {
		for(int i = 0; i < grid.length; i++) {
			if(grid[i][0] == grid[i][1] && grid[i][1] == grid[i][2]) {
				return grid[i][0];
			}
			if(grid[0][i] == grid[1][i] && grid[1][i] == grid[2][i]) {
				return grid[0][i];
			}
		}
		
		return Status.normal;
	}
	
	private Status checkDiagonals() {
		int xTotal = 0, oTotal = 0;
		
		for(int i = 0, j = 0; i < grid.length; i++, j++) {
			switch(grid[i][j]) {
			case x: xTotal++;
					break;
			case o: oTotal++;
					break;
			}
		}
		
		if(xTotal == 3) {
			return Status.x;
		}
		if(oTotal == 3) {
			return Status.o;
		}
		
		xTotal = oTotal = 0;
		
		for(int i = 2, j = 0; i >= 0; i--, j++) {
			switch(grid[i][j]) {
			case x: xTotal++;
					break;
			case o: oTotal++;
					break;
			}
		}
		
		if(xTotal == 3) {
			return Status.x;
		}
		if(oTotal == 3) {
			return Status.o;
		}
		
		return Status.normal;
	}
	
	private boolean checkGridFull() {
		for(int i = 0; i < grid.length; i++) {
			for(int j = 0; j < grid[i].length; j++) {
				if(grid[i][j] == Status.normal) {
					return false;
				}
			}
		}
		
		return true;
	}

}
