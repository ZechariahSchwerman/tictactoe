package Apprenticeship.TicTacToe;

public class StringOutput {

	TicTacToe game;
	
	StringOutput(TicTacToe game){
		this.game = game;
	}

	public String printGrid() {
		String output = "";
		
		output += " ___ ___ ___ \n";
		
		
		
		for(int j = 0; j < game.getGridSize(); j++) {

			output += "|";
			
			for(int i = 0; i < game.getGridSize(); i++) {
				
				output += " " + (i + j*game.getGridSize()) + " |";
				
				
			}
			
			output += "\n";
			
			output +="|";
			
			for(int i = 0; i < game.getGridSize(); i++) {
				
				output += " ";
				
				switch(game.getXYPositionStatus(i, j)) {
				case normal: output += " ";
						break;
				case x: output += "x";
				        break;
				case o: output += "o";
						break;
				
				}
				
				output += " |";
					
			}
			
			output +="\n";
			
			output += "|___|___|___|\n";
			
						
		}
		
		
		return output;
	}
	
	public String printStatus() {
		String output = "";
		
		switch(game.getGameStatus()) {
		case normal:
				break;
		case x: output += "x won\n";
		        break;
		case o: output += "o won\n";
				break;
		case gridFull: output += "grid full\n";

		}
		
		return output;
	}
	
	
	
}
