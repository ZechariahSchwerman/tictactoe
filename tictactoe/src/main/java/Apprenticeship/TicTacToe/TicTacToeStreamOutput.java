package Apprenticeship.TicTacToe;

import java.io.IOException;
import java.io.OutputStream;

public class TicTacToeStreamOutput {

	private final OutputStream out;
	
	private final StringOutput stringOutput;
	
	TicTacToe game;
	
	TicTacToeStreamOutput(TicTacToe game, OutputStream out) {
		
		this.stringOutput = new StringOutput(game);
		this.out = out;
		this.game = game;
		
		
	}
	
	public void render() {
		
		String output = "";
		
		output += stringOutput.printGrid();
		
		output += stringOutput.printStatus();
		
		try {
			out.write(output.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	
}
