package Apprenticeship.TicTacToe;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import junit.framework.TestCase;

public class TestStringOutput extends TestCase {
	
	/**
	 * tests drawing of blank tictactoe grid
	 */

	public void testDrawBlankGrid() {		
		TicTacToe game = new TicTacToe();
		
		StringOutput stringOutput = new StringOutput(game);
		
		assertEquals(stringOutput.printGrid(),  " ___ ___ ___ \n"
				                               +"| 0 | 1 | 2 |\n"
				                               +"|   |   |   |\n"
				                               +"|___|___|___|\n"
				                               +"| 3 | 4 | 5 |\n"
				                               +"|   |   |   |\n"
				                               +"|___|___|___|\n"
				                               +"| 6 | 7 | 8 |\n"
				                               +"|   |   |   |\n"
				                               +"|___|___|___|\n");
	}
	
	/**
	 * tests grid print with x in 0
	 */
	public void testXPosition0() {
		
		TicTacToe game = new TicTacToe();
		
		game.move(0);
		
		StringOutput stringOutput = new StringOutput(game);
		
		assertEquals(stringOutput.printGrid(),  " ___ ___ ___ \n"
				+                               "| 0 | 1 | 2 |\n"
				                               +"| x |   |   |\n"
				                               +"|___|___|___|\n"
				                               +"| 3 | 4 | 5 |\n"
				                               +"|   |   |   |\n"
				                               +"|___|___|___|\n"
				                               +"| 6 | 7 | 8 |\n"
				                               +"|   |   |   |\n"
				                               +"|___|___|___|\n");
	}
	
	/**
	 * tests grid print with x in 0 and o in 1
	 */
	public void testXPosition0OPosition1() {
		
		TicTacToe game = new TicTacToe();
		
		game.move(0);
		game.move(1);
		
		StringOutput stringOutput = new StringOutput(game);
		
		assertEquals(stringOutput.printGrid(),  " ___ ___ ___ \n"
				+                               "| 0 | 1 | 2 |\n"
				                               +"| x | o |   |\n"
				                               +"|___|___|___|\n"
				                               +"| 3 | 4 | 5 |\n"
				                               +"|   |   |   |\n"
				                               +"|___|___|___|\n"
				                               +"| 6 | 7 | 8 |\n"
				                               +"|   |   |   |\n"
				                               +"|___|___|___|\n");
	}
	
	/**
	 * tests printing of status x won
	 */

	public void testXWonMessage() {

		
		TicTacToe game = new TicTacToe();
		
		game.move(0);
		game.move(1);
		game.move(3);
		game.move(2);
		game.move(6);
		
		StringOutput stringOutput = new StringOutput(game);
		
		assertEquals(stringOutput.printStatus(), "x won\n");
	}
	
	/**
	 * tests printing of status o won
	 */

	public void testOWonMessage() {

		
		TicTacToe game = new TicTacToe();
		
		game.move(1);
		game.move(0);
		game.move(3);
		game.move(4);
		game.move(6);
		game.move(8);
		
		StringOutput stringOutput = new StringOutput(game);
		
		assertEquals(stringOutput.printStatus(), "o won\n");
	}
	
	/**
	 * tests printing of status grid full
	 */

	public void testGridFullMessage() {

		
		TicTacToe game = new TicTacToe();
		
		game.move(0); //x
		game.move(1); //o
		game.move(2); //x
		game.move(4); //o
		game.move(3); //x
		game.move(5); //o
		game.move(8); //x
		game.move(6); //o
		game.move(7); //x
		
		StringOutput stringOutput = new StringOutput(game);
		
		assertEquals(stringOutput.printStatus(), "grid full\n");
	}
	
	
}
