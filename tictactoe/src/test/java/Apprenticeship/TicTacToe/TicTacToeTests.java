package Apprenticeship.TicTacToe;

import java.io.*;
import java.math.BigInteger;
import java.util.Arrays;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class TicTacToeTests 
    extends TestCase
{
	/**
	 * tests drawing of blank tictactoe grid
	 */

	public void testGetOpenPositions() {

		
		TicTacToe game = new TicTacToe();
		
		assertEquals(game.getOpenPositions(), Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));
		
	}
	
	public void testGetGridSize() {

		
		TicTacToe game = new TicTacToe();
		
		assertEquals(game.getGridSize(), 3);
	}
	
	public void testGetXYPositionStatus() {

		
		TicTacToe game = new TicTacToe();
		
		assertEquals(game.getXYPositionStatus(0,0), Status.normal);
	}
	
	public void testNormalGetGameStatus() {
		
		TicTacToe game = new TicTacToe();
		
		assertEquals(game.getGameStatus(), Status.normal);
	}
	
	public void testXWonGetGameStatus() {
		TicTacToe game = new TicTacToe();
		
		game.move(0);
		game.move(1);
		game.move(3);
		game.move(2);
		game.move(6);
		
		assertEquals(game.getGameStatus(), Status.x);
	}
	
	public void testOWonGetGameStatus() {
		TicTacToe game = new TicTacToe();
		
		game.move(1);
		game.move(0);
		game.move(3);
		game.move(4);
		game.move(6);
		game.move(8);
		
		assertEquals(game.getGameStatus(), Status.o);
	}
	
	public void testGridFullGetGameStatus() {
		TicTacToe game = new TicTacToe();
		
		game.move(0); //x
		game.move(1); //o
		game.move(2); //x
		game.move(4); //o
		game.move(3); //x
		game.move(5); //o
		game.move(8); //x
		game.move(6); //o
		game.move(7); //x
		
		assertEquals(game.getGameStatus(), Status.gridFull);
	}
	
	public void testHorizontalWinLogic() {
		
		TicTacToe game = new TicTacToe();
		
		game.move(0);
		game.move(3);
		game.move(1);
		game.move(6);
		game.move(2);
		
		assertEquals(game.getGameStatus(), Status.x);
	}
	
	public void testVerticalWinLogic() {
		
		TicTacToe game = new TicTacToe();
		
		game.move(0);
		game.move(1);
		game.move(3);
		game.move(2);
		game.move(6);
		
		assertEquals(game.getGameStatus(), Status.x);
	}
	
	public void testTopLeftBottomRightWinLogic() {
		
		TicTacToe game = new TicTacToe();
		
		game.move(0);
		game.move(3);
		game.move(4);
		game.move(6);
		game.move(8);
		
		assertEquals(game.getGameStatus(), Status.x);
	}
	
	public void testTopRightBottomLeftWinLogic() {
		
		TicTacToe game = new TicTacToe();
		
		game.move(2);
		game.move(3);
		game.move(4);
		game.move(7);
		game.move(6);
		
		assertEquals(game.getGameStatus(), Status.x);
	}
	
	public void testPreventMovesAfterEnd() {
		
		TicTacToe game = new TicTacToe();
		
		game.move(2);
		game.move(3);
		game.move(4);
		game.move(7);
		game.move(6);
		
		
		assertEquals(game.move(0), Status.x);
	}
	
}
