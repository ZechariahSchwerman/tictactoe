package Apprenticeship.TicTacToe;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import junit.framework.TestCase;

public class TestTicTacToeStreamOutput 
		extends TestCase {
	
	public void testTicTacToeStreamOutput() throws IOException {
		final OutputStream out = new ByteArrayOutputStream();
		
		TicTacToe game = new TicTacToe();
		
		TicTacToeStreamOutput temp = new TicTacToeStreamOutput(game, out);
		
		temp.render();
		
		assertEquals(out.toString(),  " ___ ___ ___ \n"
				          +"| 0 | 1 | 2 |\n"
				          +"|   |   |   |\n"
				          +"|___|___|___|\n"
				          +"| 3 | 4 | 5 |\n"
				          +"|   |   |   |\n"
				          +"|___|___|___|\n"
				          +"| 6 | 7 | 8 |\n"
				          +"|   |   |   |\n"
				          +"|___|___|___|\n");
	}
	

}
