This file tells how to use this project.

BUILDING THE PROJECT

To build the project use Maven. On windows install
chocolatey using the instructions here:

https://chocolatey.org/install

then run an administrator Powershell by
searching for Powershell on the start menu.
Be sure to user Powershell itself and not the Powershell ISE
right clicking, and selecting to run as Administrator.

In the admin Powershell run

choco install maven

to install maven.


Now open a normal powershell (so you don't do anything
accidently with admin privileges) and navigate to the directory
the repository is cloned into.Navigate Powershell into
the tictactoe directory inside the repository.

To produce a .jar file execute

mvn package

The game can be executed from the .jar by running

java -cp target/TicTacToe-0.0.1-SNAPSHOT.jar Apprenticeship.TicTacToe.TicTacToe

PLAYING THE GAME

The game is tic-tac-toe against a computer.
Player is x and goes first. Computer is o.
Grid squares are numbered 0-8 starting in the top left
and going right and down.

Get 3 in a row to win.

Game will ask if you want to replay after a game ends.